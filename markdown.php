<!-- Hallo
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
</head>
-->
<div class="navmenu navmenu-default navmenu-fixed-left" id="tutorial" style="border-left-width: 3px;">

    <div id="markdown"></div>

</div>





<div class="canvas" style="background-color:">

    <div class="navbar navbar-default navbar-fixed-top" style="padding-top: 0px; margin-left: 6px;">
        <button id="resize" type="button" class="navbar-toggle" data-toggle="offcanvas" data-recalc="false" data-target=".navmenu" data-canvas=".canvas" >
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>

    </div>
    <div class="container-fluid" style="padding-left: 60px">

        <form class="navbar-form navbar-left" role="search">
            <span class="title">Markdown-HTML Live Preview Editor</span>
            <button type="button" id="Deutsch" class="btn btn-default" onclick="formdeutsch();">Deutsch</button>
            <button type="button" id="Englisch" class="btn btn-default" onclick="formenglisch();">English</button>
            <button type="button" id="Französisch" class="btn btn-default" onclick="formfranzoesisch();">Français</button>
            <div class="form-group ui-widget easy">
                <input type="text" id="Suche" list="dlist" value="" class="form-control" placeholder="Suchbegriff eingeben..." aria-label="Input group example" aria-describedby="btnGroupAddon" />
            </div>
            <div class="form-group ui-widget">
                <label for="oxid">OXID:</label>
                <input id="oxid" type="text" class="form-control" width="285px" disabled data-lang="de">
            </div>
            <div class="form-group ui-widget">
                <label for="user">USER/RIGHTS:</label>
                <input id="user" type="text" class="form-control"  disabled data-lang="de">
            </div>

        </form>
        <button type="button" id="logout" class="btn btn-danger pull-right" style="margin-top: 7px; margin-right: 8px;" onclick="logout();">LOGOUT</button>
        <button type="button" id="verweis" class="btn btn-default pull-right" style="left: 30px; background-color: rosybrown; margin-top: 7px;  margin-right: 2px;" onclick="datatable();">Tabelle</button>
    </div>


    <div class="container-fluid">
        <div class="col-lg-6 ganzeHoehe" id="a">
            <textarea id="md" onkeyup="parseMD()" name="md" title="md" onpaste="parseMD() class="form-control"></textarea>
        </div>
        <div class="ganzeHoehe ausgeblendet" id="b">
            <div id="htmlOutput" class="neu"></div>
        </div>
        <button class="btn btn-default" style="margin-left: 14px;margin-top: 5px;" type="submit"  onclick="savetodb()">Speichern</button>
        <button type="button" id="todo" class="btn btn-danger" style="margin-top: 5px;" type="submit" data-toggle="modal" data-target="#myModal" onclick="ToDo()">ToDo</button>
        <button type="button" class="btn btn-info btn-lg pull-right" style="margin-top: 5px; margin-right: 8px;" data-toggle="modal" data-target="#myModal" onclick="modal()">Quick Preview</button>
        <button type="button" id="preview" class="btn btn-info btn-lg pull-right" style="margin-top: 5px; margin-right: 2px;" onclick="preview()">Preview Website</button>

    </div>

</div>

<div class="container">
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div id="modal-preview" class="modal-body">
                    <p></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
</div>


<script>


    $('#myModal').on('hidden.bs.modal', function (e) {
        console.log(e);
        $('#modal-preview').html('');
        $('h4.modal-title').html('');
    });

   // Event bei Enter-Klick
   /* $(function(){
        var input = document.getElementById("Suche");
       input.addEventListener("keyup", function(event){
          event.preventDefault();
          if(event.keyCode === 13){
              var Inhalt = $("#Suche").val();
              alert(Inhalt);


          }
       });
    });
    */

    $(function(){

        var email = localStorage.name;
        // console.log(email);
        $.ajax({
            type: 'get',
            url: 'ArtikelID.php',
            data: {
                fnc: 'userRights',
                email: email,

            },
            success: function(result){
                var res = jQuery.parseJSON(result);

                console.log(res);
                var username = res[0]['OXFNAME'];
                var rights = res[0]['OXDEVRIGHTS'];
                console.log(username);

                $("#user").val(username+' ('+rights+')');
                window.localStorage.setItem('role',rights);
                window.localStorage.setItem('UserName', username);
                if(localStorage.role === 'Translator-Check'){
                   // $('#Suche').attr('disabled','');
                    $('#Suche').attr('style','visibility:hidden');

                }
            }

        });
    });







    Split(['#a', '#b'], {
        gutterSize: 8,
        cursor: 'col-resize'
    });

    var md = new Remarkable('full', {
        html:         true,         // Enable HTML tags in source
        xhtmlOut:     true,        // Use '/' to close single tags (<br />)
        breaks:       true,        // Convert '\n' in paragraphs into <br>
        langPrefix:   'language-',  // CSS language prefix for fenced blocks
        linkify:      true,         // autoconvert URL-like texts to links
        linkTarget:   '',           // set target to open link in

        // Enable some language-neutral replacements + quotes beautification
        typographer:  true,

        // Double + single quotes replacement pairs, when typographer enabled,
        // and smartquotes on. Set doubles to '«»' for Russian, '„“' for German.
        quotes: '“”‘’',
    });
    var my = new Remarkable('full', {
        html:         true,         // Enable HTML tags in source
        xhtmlOut:     true,        // Use '/' to close single tags (<br />)
        breaks:       true,        // Convert '\n' in paragraphs into <br>
        langPrefix:   'language-',  // CSS language prefix for fenced blocks
        linkify:      true,         // autoconvert URL-like texts to links
        linkTarget:   '',           // set target to open link in

        // Enable some language-neutral replacements + quotes beautification
        typographer:  true,

        // Double + single quotes replacement pairs, when typographer enabled,
        // and smartquotes on. Set doubles to '«»' for Russian, '„“' for German.
        quotes: '“”‘’',
    });

    $(function() {

        var options = {

            url: function (phrase) {
                return "ArtikelID.php?fnc=dataSearch&data=" + phrase;
            },


            getValue: function (element) {

                return element.OXARTNUM + " | " + element.OXTITLE;
            },

            ajaxSettings: {
                dataType: "json",
                method: "get",
                data: {
                    data: function () {
                        return $('#Suche').val();
                    }
                }

            },
            listLocation: 'data',

            theme: "round",

            list: {
                onClickEvent: function () {
                    var oxid = $("#Suche").getSelectedItemData().OXID;
                    var title = $("#Suche").getSelectedItemData().OXTITLE;
                    var oxartnum = $("#Suche").getSelectedItemData().OXARTNUM;


                    console.log(oxid);
                    console.log(title);
                    console.log(oxid + " | " + title + " | " + oxartnum);

                    $.ajax({
                        type: 'get',
                        url: 'ArtikelID.php',
                        data: {
                            fnc: 'getarticlebyoxid',
                            oxid: oxid
                        },
                        success: function (result) {

                            if (localStorage.role === 'IT-Admin' || localStorage.role === 'Translator-Check'){

                                var res = jQuery.parseJSON(result);
                                // console.log(res);

                                text = res[0]['OXDEVLONGDESC'];

                                console.log(text);
                                console.log('TEST Translator-Check!');

                                if(text=== null){
                                    text = res[0]['OXLONGDESC'];
                                }

                                $('#oxid').val();
                                $('#oxid').val(res[0]['OXID']);
                                $('#md').val('');
                                $('#md').val(text);

                                parseMD();
                            } else if (localStorage.role === 'Translator'){
                                var res = jQuery.parseJSON(result);
                                // console.log(res);


                                text = res[0]['OXLONGDESC'];

                                console.log(text);
                                console.log('TEST Translator!');

                                $('#oxid').val();
                                $('#oxid').val(res[0]['OXID']);
                                $('#md').val('');
                                $('#md').val(text);

                                parseMD();
                            }
                        },
                        error: function(result){
                            console.log('ERROR' +result);
                        }
                    });
                }
            }
        };
        $("#Suche").easyAutocomplete(options);

    });


    function savetodb() {
        console.log(localStorage.role);
        if(localStorage.role==='IT-Admin') {

            var oxid = $('#oxid').val();
            //var text = $('#md').val();
            var text = document.getElementById('htmlOutput').innerHTML;
            var lang = $('#oxid').attr('data-lang');
            var role = localStorage.role;


            $.ajax({
                type: 'get',
                url: 'ArtikelID.php',
                data: {
                    fnc: 'savetodatabank',
                    oxid: oxid,
                    text: text,
                    lang: lang,
                    role: role

                },
                success: function (response) {
                    console.log(response);
                }
            });
        }
        else if(localStorage.role==='Translator' || localStorage.role==='Translator-Check') {
            savetodbDEV();
        }

    }

    function savetodbDEV() {

        var oxid = $('#oxid').val();
        //var text = $('#md').val();
        var text = document.getElementById('htmlOutput').innerHTML;
        var lang = $('#oxid').attr('data-lang');
        var role = localStorage.role;

        $.ajax({
            type: 'get',
            url: 'ArtikelID.php',
            data: {
                fnc: 'savetodatabankDEV',
                oxid: oxid,
                text: text,
                lang: lang,
                role: role

            },
            success: function (response) {
                console.log(response);
            }
        });


    }

    function parseMD() {

        document.getElementById('htmlOutput').className = 'preview markdown-body';
        document.getElementById('htmlOutput').innerHTML = md.render(document.getElementById('md').value);

    }

    function formdeutsch(){
        $('#oxid').attr('data-lang','de');
        var oxid =  $('#oxid').val();
        $.ajax({
            type: 'get',
            url: 'ArtikelID.php',
            data: {
                fnc: 'getarticlebyoxid',
                oxid: oxid
            },
            success: function (result) {

                if (localStorage.role === 'IT-Admin' || localStorage.role === 'Translator-Check'){

                    var res = jQuery.parseJSON(result);
                    if(res[0] === undefined ){
                        alert('Kein Artikel ausgewählt!');
                        return;
                    }

                    var text = res[0]['OXDEVLONGDESC'];

                    console.log(text);
                    console.log('TEST Translator-Check-DEUTSCHBUTTON!');

                    if(text=== null){
                        text = res[0]['OXLONGDESC'];
                    }

                    $('#oxid').val('');
                    $('#oxid').val(res[0]['OXID']);
                    $('#md').val('');
                    $('#md').val(text);

                    parseMD();
                } else if (localStorage.role === 'Translator'){
                    var res = jQuery.parseJSON(result);
                    if(res[0] === undefined ){
                        alert('Kein Artikel ausgewählt!');
                        return;
                    }


                    var text = res[0]['OXLONGDESC'];

                    console.log(text);
                    console.log('TEST TranslatorBUTTONDEUTSCH!');

                    $('#oxid').val();
                    $('#oxid').val(res[0]['OXID']);
                    $('#md').val('');
                    $('#md').val(text);

                    parseMD();
                }


            }
        });
    }

    function formenglisch(){
        $('#oxid').attr('data-lang','en');
        var oxid =  $('#oxid').val();
        $.ajax({
            type: 'get',
            url: 'ArtikelID.php',
            data: {
                fnc: 'getarticlebyoxid',
                oxid: oxid
            },
            success: function (result) {


                if (localStorage.role === 'IT-Admin' || localStorage.role === 'Translator-Check'){

                    var res = jQuery.parseJSON(result);
                    // console.log(res);
                    if(res[0] === undefined ){
                        alert('Kein Artikel ausgewählt!');
                        return;
                    }

                    var text = res[0]['OXDEVLONGDESC_1'];

                    console.log(text);
                    console.log('TEST Translator-Check-ENGLISCHBUTTON!');

                    if(text=== null){
                        text = res[0]['OXLONGDESC_1'];
                    }

                    $('#oxid').val('');
                    $('#oxid').val(res[0]['OXID']);
                    $('#md').val('');
                    $('#md').val(text);

                    parseMD();
                } else if (localStorage.role === 'Translator'){
                    var res = jQuery.parseJSON(result);
                    console.log(res);
                    if(res[0] === undefined ){
                        alert('Kein Artikel ausgewählt!');
                        return;
                    }

                    var text = res[0]['OXLONGDESC_1'];

                    console.log(text);
                    console.log('TEST TranslatorBUTTONENGLISCH!');

                    $('#oxid').val();
                    $('#oxid').val(res[0]['OXID']);
                    $('#md').val('');
                    $('#md').val(text);

                    parseMD();
                }
            }
        });
    }

    function formfranzoesisch(){
        $('#oxid').attr('data-lang','fr');
        var oxid =  $('#oxid').val();
        $.ajax({
            type: 'get',
            url: 'ArtikelID.php',
            data: {
                fnc: 'getarticlebyoxid',
                oxid: oxid
            },
            success: function (result) {


                if (localStorage.role === 'IT-Admin' || localStorage.role === 'Translator-Check'){

                    var res = jQuery.parseJSON(result);
                    // console.log(res);
                    if(res[0] === undefined ){
                        alert('Kein Artikel ausgewählt!');
                        return;
                    }

                    var text = res[0]['OXDEVLONGDESC_2'];

                    console.log(text);
                    console.log('TEST Translator-Check-FRANZÖSISCHBUTTON!');

                    if(text=== null){
                        text = res[0]['OXLONGDESC_2'];
                    }

                    $('#oxid').val('');
                    $('#oxid').val(res[0]['OXID']);
                    $('#md').val('');
                    $('#md').val(text);

                    parseMD();
                } else if (localStorage.role === 'Translator'){
                    var res = jQuery.parseJSON(result);
                    console.log(res);
                    if(res[0] === undefined ){
                        alert('Kein Artikel ausgewählt!');
                        return;
                    }

                    var text = res[0]['OXLONGDESC_2'];

                    console.log(text);
                    console.log('TEST TranslatorBUTTONFRANZÖSISCH!');

                    $('#oxid').val();
                    $('#oxid').val(res[0]['OXID']);
                    $('#md').val('');
                    $('#md').val(text);

                    parseMD();
                }
            }
        });
    }
    /**
     * AKTIVIEREN!!!!!!
     * **/
    $(document).ready(function(){



        /* $('#md').on('focusout',function(){
             savetodb();
             console.log('focusout');

         }); */
        parseTutorial();



    });


    function parseTutorial(){
        document.getElementById('markdown').className = 'preview markdown-body';

        var test = null;

        $.ajax({
            type: 'get',
            url: 'TutorialMarkdown.php',
            success: function (result) {
                test = result;
                // console.log(test);
                document.getElementById('markdown').innerHTML = md.render(test);
            }
        });


    }




</script>

<?php

require dirname(__DIR__).'/bootstrap.php';


if($_GET['fnc'] == 'getarticlebyoxid'){
	
	$arrArtikel = getarticlebyoxid($_GET['oxid']);
	//$arrArtikelText = getArticleTextbyOxid($arrArtikel[0]['OXID']);
	die( json_encode(getArticleTextbyOxid($_GET['oxid'])));
}

if($_GET['fnc'] == 'getPictureByOxid'){

	$arrPicture = getPictureByOxid($_GET['oxid']);

	die( json_encode(getPictureByOxid($_GET['oxid'])));
}

if($_GET['fnc'] == 'dataTable'){

    $arrDataTable = dataTable();

    die( json_encode($arrDataTable));
}

 if($_GET['fnc'] =='userRights'){

    $arrUserRights = userRights($_GET['email']);

    die( json_encode($arrUserRights));

}

if($_GET['fnc'] == 'dataSearch'){

    $arrDataSearch = dataSearch($_GET['data']);

    die( json_encode($arrDataSearch));
}




if($_GET['fnc'] == 'savetodatabank'){


    $arrUpdateRequest = savetodatabank($_GET['oxid'],$_GET['text'],$_GET['lang'],$_GET['role']);

    die(json_encode($_GET));
}


if($_GET['fnc'] == 'savetodatabankDEV'){


    $arrUpdateRequest = savetodatabankDEV($_GET['oxid'],$_GET['text'],$_GET['lang'],$_GET['role']);

    die(json_encode($_GET));
}

if($_GET['fnc'] == 'modalForList'){
    $arrModalList = modalForList($_GET['oxid']);

    die(json_encode($arrModalList));
}

if($_GET['fnc'] == 'changeLang'){

    $UebergabeArray = explode(',',$_GET['array']);

    $arrChangeLang = changeLang($UebergabeArray);
    die(json_encode($arrChangeLang));
}


if($_GET['fnc'] == 'todo'){
    $arrToDo = todo();
    die(json_encode($arrToDo));
}





function dataSearch($data)
{
    try {
        $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb(oxDb::FETCH_MODE_ASSOC);

        $query = 'SELECT OXID,OXARTNUM,OXTITLE,OXPARENTID, CONCAT(OXID, \' \', OXARTNUM, \' \', OXTITLE) AS Response FROM oxarticles
WHERE CONCAT(OXID, \' \', OXARTNUM, \' \',OXTITLE) LIKE \'%' . $data . '%\'';


        // $query = 'select * from oxartextends where OXID =\'%'.$_GET['data'].'%\'';
        $article = $oDb->getAll($query);


    } catch (Exception $e) {
        echo 'Exception abgefangen: ', $e->getMessage(), "\n";
    }

    $Array = null;


    foreach ($article as $key) {
        // echo $key['OXPARENTID'];
        if (empty(trim($key['OXPARENTID']))) {
            $Array[] = $key;
        } elseif (!empty(trim($key['OXPARENTID']))) {
            if (!in_array($key['OXPARENTID'], $Array)) {


                $Array[] = getarticlebyoxid($key['OXPARENTID'])[0];

                // print_r(getarticlebyoxid($key['OXPARENTID'])[0]);
            }
        }

    }

    if ($Array == null) {
        $Array['Error'] = 'Leider hat ihre Suche keinen Treffer ergeben';
    }

    return array('data' => $Array, 'request' => $_GET); // Neue Funktion Data
}



function savetodatabank($oxid, $text, $lang, $role){
    try{
        $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb(oxDb::FETCH_MODE_ASSOC);

        if($lang=='de') {
            $query = 'UPDATE oxartextends SET OXLONGDESC =?,OXUSERVALTIMESTAMP=NOW(), USERVAL =? WHERE OXID =?';
        } else if($lang=='en'){
            $query = 'UPDATE oxartextends SET OXLONGDESC_1 =?,OXUSERVALTIMESTAMP=NOW(), USERVAL_1=? WHERE OXID =?';
        } else if($lang=='fr'){
            $query = 'UPDATE oxartextends SET OXLONGDESC_2 =?,OXUSERVALTIMESTAMP=NOW(), USERVAL_2=? WHERE OXID =?';
        }


        $arrUpdateRequest = array($text,$role,$oxid);
        $blUpdateRequest = $oDb->execute($query,$arrUpdateRequest);




    } catch(Exception $e){
        echo 'Exception abgefangen: ',  $e->getMessage(), "\n";
    }
    return $blUpdateRequest;
}


function savetodatabankDEV($oxid, $text, $lang, $role){
    try{
        $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb(oxDb::FETCH_MODE_ASSOC);

        if($lang=='de') {
            if($role=='Translator') {
                $query = 'UPDATE oxartextends SET OXDEVLONGDESC =?,OXUSERCHANGETIMESTAMP=NOW(), USERCHANGE =? WHERE OXID =?';
            } else if($role=='Translator-Check'){
                $query = 'UPDATE oxartextends SET OXDEVLONGDESC =?,OXUSERCHECKTIMESTAMP=NOW(), USERCHECK =? WHERE OXID =?';
            }
        } else if($lang=='en') {
            if ($role == 'Translator') {
                $query = 'UPDATE oxartextends SET OXDEVLONGDESC_1 =?,OXUSERCHANGETIMESTAMP=NOW(), USERCHANGE_1 =? WHERE OXID =?';
            } else if ($role == 'Translator-Check') {
                $query = 'UPDATE oxartextends SET OXDEVLONGDESC_1 =?,OXUSERCHECKTIMESTAMP=NOW(), USERCHECK_1 =? WHERE OXID =?';
            }
        } else if($lang=='fr') {
            if ($role == 'Translator') {
                $query = 'UPDATE oxartextends SET OXDEVLONGDESC_2 =?,OXUSERCHANGETIMESTAMP=NOW(), USERCHANGE_2 =? WHERE OXID =?';
            } else if ($role == 'Translator-Check') {
                $query = 'UPDATE oxartextends SET OXDEVLONGDESC_2 =?,OXUSERCHECKTIMESTAMP=NOW(), USERCHECK_2 =? WHERE OXID =?';
            }
        }




        $arrUpdateRequest = array($text,$role,$oxid);
        $blUpdateRequest = $oDb->execute($query,$arrUpdateRequest);




    } catch(Exception $e){
        echo 'Exception abgefangen: ',  $e->getMessage(), "\n";
    }
    return $blUpdateRequest;
}

/*
die( json_encode(array('data'=>$Array,'request' => $_GET)));
*/






function getarticlebyoxid($oxid){
    try{
        $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb(oxDb::FETCH_MODE_ASSOC);

        $query= 'SELECT * FROM oxarticles WHERE OXID =?';



        $arrParameter = array($oxid);

        // $query = 'select * from oxartextends where OXID =\'%'.$_GET['data'].'%\'';
        $article = $oDb->getAll($query,$arrParameter);


    } catch(Exception $e){
        echo 'Exception abgefangen: ',  $e->getMessage(), "\n";
    }
    return $article;

}

function getArticleTextbyOxid($oxid){

    try{
        $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb(oxDb::FETCH_MODE_ASSOC);

        $query= 'SELECT * FROM oxartextends WHERE OXID =?';


        $arrParameter = array($oxid);
        // $query = 'select * from oxartextends where OXID =\'%'.$_GET['data'].'%\'';
        $arrArticleText = $oDb->getAll($query,$arrParameter);


    } catch(Exception $e){
        echo 'Exception abgefangen: ',  $e->getMessage(), "\n";
    }
    return $arrArticleText;

}


function getPictureByOxid($oxid) {

    try {
        $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb(oxDb::FETCH_MODE_ASSOC);

        $query = 'SELECT OXPIC1 FROM oxarticles WHERE OXID  =?';
        $arrParameter = array($oxid);
        $arrPicture = $oDb->getAll($query,$arrParameter);
    } catch(Exception $e){
        echo 'Exception abgefangen: ', $e->getMessage(), "\n";
    }
    return $arrPicture;
}

function modalForList($oxid) {
    try {
        $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb(oxDb::FETCH_MODE_ASSOC);
        $query = 'SELECT oxarticles.OXPIC1, oxartextends.OXLONGDESC, oxartextends.OXLONGDESC_1, oxartextends.OXLONGDESC_2 
                  FROM oxarticles,oxartextends 
                  WHERE oxarticles.OXID = oxartextends.OXID AND oxarticles.OXID  =?';

        $arrParameter = array($oxid);
        $arrModalList = $oDb->getAll($query,$arrParameter);
    } catch(Exception $e){
        echo 'Exception abgefangen: ', $e->getMessage(), "\n";
    }
    return $arrModalList;
}

function dataTable(){
    try {
        $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb(oxDb::FETCH_MODE_ASSOC);

        //$query = 'SELECT OXID, OXARTNUM, OXTITLE, OXSHORTDESC, OXPARENTID FROM `oxarticles`';
        $query = 'SELECT oxarticles.OXID, OXARTNUM, OXTITLE, OXTITLE_1, OXTITLE_2, OXLONGDESC, OXLONGDESC_1, OXLONGDESC_2, OXDEVLONGDESC, OXDEVLONGDESC_1, OXDEVLONGDESC_2, OXSHORTDESC, OXSHORTDESC_1, OXSHORTDESC_2, OXPARENTID, OXUSERVALTIMESTAMP, OXUSERCHANGETIMESTAMP, OXUSERCHECKTIMESTAMP, USERVAL, USERVAL_1, USERVAL_2, USERCHANGE, USERCHANGE_1, USERCHANGE_2, USERCHECK, USERCHECK_1, USERCHECK_2 
                  FROM oxarticles
                  LEFT JOIN oxartextends ON oxarticles.OXID = oxartextends.OXID
                  WHERE OXPARENTID=""';
        $arrDataTable = $oDb->getAll($query);
    } catch(Exception $e) {
        echo 'Exception abgefangen: ',$e->getMessage(), "\n";
    }
    return $arrDataTable;
}

function userRights($email){
    try{
        $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb(oxDb::FETCH_MODE_ASSOC);
        $query = 'SELECT * FROM oxuser WHERE OXUSERNAME  =?';
        $arrParameter = array($email);
        $arrUserRights = $oDb->getAll($query,$arrParameter);
    } catch(Exception $e) {
        echo 'Exception abgefangen: ',$e->getMessage(), "\n";
    }
    return $arrUserRights;
}

function changeLang($array){
    try{

        $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb(oxDb::FETCH_MODE_ASSOC);

        $arrToString = "'".implode("','",$array)."'";

         $query = 'SELECT
                    oxarticles.OXID,
                    OXARTNUM,
                    OXTITLE,
                    OXTITLE_1,
                    OXTITLE_2,
                    OXSHORTDESC,
                    OXSHORTDESC_1,
                    OXSHORTDESC_2,
                    OXPARENTID,
                    OXUSERVALTIMESTAMP,
                    OXUSERCHANGETIMESTAMP,
                    OXUSERCHECKTIMESTAMP,
                    USERVAL,
                    USERVAL_1,
                    USERVAL_2,
                    USERCHANGE,
                    USERCHANGE_1,
                    USERCHANGE_2,
                    USERCHECK,
                    USERCHECK_1,
                    USERCHECK_2 
                  FROM
	                oxarticles
	              LEFT JOIN oxartextends ON oxarticles.OXID = oxartextends.OXID
                  WHERE oxarticles.OXID IN ('.$arrToString.')';
        // $arrParameter = array($arrToString);
        $arrChangeLang = $oDb->getAll($query);


    } catch(Exception $e){
        echo 'Exception abgefangen ', $e->getMessage(), "\n";
    }
    return $arrChangeLang;
}

function todo(){
    try {
        $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb(oxDb::FETCH_MODE_ASSOC);

        $query = 'SELECT * FROM oxartextends';
        $arrToDo = $oDb->getAll($query);
    } catch(Exception $e) {
        echo 'Exception abgefangen', $e->getMessage(), "\n";
    }
    return $arrToDo;
}

?>


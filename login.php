<?php
include ('../bootstrap.php');
use OxidEsales\EshopCommunity\Application\Model;
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');

$sess_cache_expire    = 150;                      // in Minuten
$sess_cookie_lifetime = $sess_cache_expire * 60;  // in Sekunden
$sess_cookie_lifetime = 0;                        // bis der Browser geschlossen wird

//echo '<pre>';
//print_r($_SERVER);
//die();
//if(!empty($_SESSION) )
session_cache_expire($sess_cache_expire);
session_cache_limiter('nocache');
session_set_cookie_params($sess_cookie_lifetime);
session_name("Bodynova MD-Editor");
session_start();



$oBn = oxNew(\OxidEsales\Eshop\Application\Model\User::class);


if($_POST['fnc']=='debug'){
    die(json_encode($_POST));
}

if($_GET['fnc']=='showsession'){
    die(json_encode($_SESSION));
}



 //die('HALLO');



if(isset($_POST['email']) && isset($_POST['password']) && $_POST['fnc']=='login')  {
    $user = $_POST['email'];
    $pw = $_POST['password'];



  $arrReturn=array();
  $arrReturn['email']=$_POST['email'];
  $arrReturn['password']=$_POST['password'];
  $arrReturn['remember']=$_POST['remember'];

    try {

        /**
         * Login mit user,pw mit Funktion login() aus der Klasse OxUser
         */

        $arrReturn['login']=$oBn->login($user,$pw);

        /**
         * Magicmethode Vergleich Userrechte..
         * Neuer Eintrag für das Returnarray (rawValue von oxrights)
         */
        if($oBn->oxuser__oxrights->rawValue=='malladmin'){
            $arrReturn['usr']=$oBn->oxuser__oxrights->rawValue;
            $_SESSION['rights'] = 'admin';
        }/**
         * Ansonsten neuer Eintrag für das Returnarray : Kein Admin
         * Logge User aus
         */
        else {
            $arrReturn['error']='Kein Admin';
            $arrReturn['usr']=$oBn->oxuser__oxrights->rawValue;
            $oBn->logout();
            $_SESSION=array();
            session_destroy();
            die(json_encode($arrReturn));
        }


        /**
         * Falls Login true, übertrage der Session den Wert 'name'
         * und erstelle neuen Arrayeintrag 'session'
         */
        if($arrReturn['login']) {
            // $_SESSION['rights'] = $_POST[''];
            $_SESSION['name'] = $_POST['email'];
            $sessid = $_SESSION['name'];
            $arrReturn['session']=$_SESSION;
        }


        die(json_encode($arrReturn));
        // header("Location:index.php");
        // include 'ArtikelID.php';

      //  echo('Richtige Eingabe');

        // exit();

    } catch(Exception $e){

        //$arrReturn['error']=$e;
        //$_SESSION['error'] = $e;
        if(!empty($_SESSION)){
            $_SESSION = array();
        }

        $_SESSION['name'] = $_POST['email'];

        if(!empty($_SESSION['start'])) {
            $_SESSION['start'] = date('d.m.Y H:i:s:u');
        }


        $arrReturn['email']=$oBn->checkIfEmailExists($user);
       //$_SESSION


        // $_SESSION['timestamp'] = time();
        // $_SESSION['started'] = $_SERVER['REQUEST_TIME'];

        $arrReturn['usr']='noreg';
        $arrReturn['session']=$_SESSION;
        $arrReturn['login']=false;
        die(json_encode($arrReturn));

    }

}




/*

$user = $_POST['email'];

$pw = $_POST['password'];




try {
    $oBn->login($user,$pw);
    include 'index.php';


} catch(Exception $e){

   // include 'markdown.php';
    die();
}
*/
?>

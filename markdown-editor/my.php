<?php

require dirname(__DIR__,3).'/vendor/autoload.php';

// Fehler Anzeige eingeschaltet
error_reporting(E_ALL | E_NOTICE | E_DEPRECATED);
ini_set('display_errors', 1);


?>
<!DOCTYPE html>
<html lang="en">

<head>
	<title>Markdown-HTML Live Preview Editor</title>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- JQuery -->
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	
	<!-- Bootstrap -->
	<!-- Das neueste kompilierte und minimierte CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<!-- Optionales Theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
	<!-- Das neueste kompilierte und minimierte JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<!-- Markdown Github Projekt -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.3/normalize.css">
	<link rel="stylesheet" href="css/github-markdown.css">
	<!-- Easy Autocomplete Github Projekt -->
	<link rel="stylesheet" href="../dist/easy-autocomplete.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<script src="js/jquery.easy-autocomplete.min.js"></script>
	<!-- eigene Sachen -->
	<link rel="stylesheet" href="css/my.css">
	
	<!-- Inline Script im Head damit die Funktionalität schon vor dem Seitenaufbau zur Verfügung steht -->
	<script>
		$(function(){
			var options = {
				/**
				 *
				 * auswertung der Usereingabe und absetzen des Http_GET Request an form.php
				 * zuvor validierung der Sprachauswahl de als Standarteinstellung berücksichtigt.
				 *
				 * */
				url: function(phrase) {
					// feststellen der Sprache
					var sprache = $('#sprache').val();
					// aufbau des URL Request durch Parameter zuweisung und Rückgabe an unbenannte Funktion
					return "form.php?search=" + phrase + "&lang=" + sprache + "&fnc=suche&format=json";
				},
				/**
				 *
				 * Value des Search Fields bis eingabe vorgenommen wird
				 *
				 * */
				placeholder: "Suchbegriff eingeben ...",
				/**
				 *
				 * Value des Search Fields, wenn Auswahl getroffen wurde
				 *
				 * */
				getValue: "OXARTNUM",
				/**
				 *
				 * Einstellung: Wo der Suchbegriff mit dem Ergebnis matched, wird dies hervorgehoben
				 *
				 * */
				highlightPhrase: true,
				/**
				 *
				 * Zeitverzögerung bis die Abfrage abgeschickt wird bei Usereingabe
				 *
				 * */
				requestDelay: 500,
				/**
				 *
				 * Objekt list Array von Objekten für die Auswahlliste
				 *
				 * */
				list:{
					/**
					 *
					 * Objekt für die Animation bei der Darstellung der Liste
					 *
					 * */
					showAnimation: {
						type: "fade", //normal|slide|fade
						time: 400,
						callback: function() {
						
						}
					},
					/**
					 *
					 * Trigger der ausgelöst wird, wenn die Liste geladen wird
					 *
					 * */
					onLoadEvent:function(){
					
					},
					/**
					 *
					 * Trigger der ausgelöst wird, wenn der User eine Auswahl getroffen hat
					 *
					 * */
					onClickEvent: function () {
						var oxid   = $("#search").getSelectedItemData().OXID;
						var title  = $("#search").getSelectedItemData().Title;
						var artnum = $("#search").getSelectedItemData().OXARTNUM;
						var lang   = $('#sprache').val();
						

						console.log(artnum + " | "  + title); //+ oxid + " | "
						
						$.ajax({
							type:'get',
							url:'form.php',
							data:{
								fnc  : 'getArtikel',
								OXID : oxid,
								lang : lang
							},
							beforeSend:function(){
								//launchpreloader();
								//DEBUG:
								//console.log(item);
							},
							complete:function(){
								//stopPreloader();
							},
							success:function(result){
								// DEBUG:
								//console.log(result);
								console.log('Sprache: ' + lang);
								//console.log(result);
								var res = jQuery.parseJSON(result);
								console.log(res);
								
								var text = null;
								
								if( lang == 0 ){
									text = res.Artikeltext['OXLONGDESC'];
								}else if( lang == 1 ){
									text = res.Artikeltext['OXLONGDESC_1'];
								}else if( lang == 2 ){
									text = res.Artikeltext['OXLONGDESC_2'];
								}
								$('#oxid').val();
								$('#oxid').val(res.Artikel['OXID']);
								$('#md').html('');
								$('#md').html(text);

								parseMD();
							}
						});
					}
				},
				/**
				 *
				 * Template Objekt als Array von Objekten
				 *
				 * */
				template: {
					type: "custom",
					method: function(value, item) {
						//DEBUG:
						console.log(item);
						//console.log(value);
						return  item.OXARTNUM + " | "  + item.Title ; //+ item.OXID + " | "
					}
				}
				
			};
			
			$("#search").easyAutocomplete(options);
			
			$('#sprache').on('change',function(){
				
				var lang = $('#sprache').val();
				var oxid = $('#oxid').val();
				
				$.ajax({
					type: 'get',
					url: 'form.php',
					data: {
						fnc  : 'getArtikel',
						OXID : oxid,
						lang : lang
					},
					beforeSend: function () {
						//launchpreloader();
						//DEBUG:
						//console.log(item);
					},
					complete: function () {
						//stopPreloader();
					},
					success: function (result) {
						var res = jQuery.parseJSON(result);
						
						if( lang == 0 ){
							text = res.Artikeltext['OXLONGDESC'];
						}else if( lang == 1 ){
							text = res.Artikeltext['OXLONGDESC_1'];
						}else if( lang == 2 ){
							text = res.Artikeltext['OXLONGDESC_2'];
						}
						$('#md').html('');
						$('#md').html(text);
						
						parseMD();
					}
				});
				
			});
			
		});
	</script>
</head>

<body>



<!-- Navigation -->
<nav class="navbar navbar-default">
	<div class="container-fluid">
		<!-- Titel und Schalter werden für eine bessere mobile Ansicht zusammengefasst -->
		<div class="navbar-header">
			<a class="navbar-brand" href="#">Bodynova Markdown</a>
		</div>
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<form class="navbar-form navbar-left" role="search">
				<!-- Suchfeld -->
				<div class="form-group">
					<input id="search" type="text" class="form-control" placeholder="Suchen">
				</div>
				<!-- <button type="submit" class="btn btn-default">Los</button> -->
				<!-- Sprachen Wechsler -->
				<div class="form-group ui-widget">
					<label for="sprache">Sprache: </label>
					<select class="form-control" name="sprache" id="sprache">
						<option value="0">Deutsch</option>
						<option value="1">Englisch</option>
						<option value="2">Französisch</option>
					</select>
				</div>
				<!-- OXID Containter -->
				<div class="form-group ui-widget">
					<label for="oxid">OXID:</label>
					<input id="oxid" type="text" class="form-control" width="285px" disabled>
				</div>
				<!-- OXID User -->
				<div class="form-group ui-widget">
					<label for="user">User:</label>
					<select class="form-control" name="user" id="user">
						<option value="0">Bitte wählen</option>
						<option value="1">André</option>
						<option value="2">Christian</option>
						<option value="3">Malik</option>
					</select>
				</div>
			</form>
		</div>
	</div>
</nav>

<!-- Content -->
<div class="container">
	<!-- Linke Textbox -->
	<div id="a" class="split split-horizontal content">
		<textarea id="md" name="md" title="md" onkeyup="parseMD()" class="form-control">
		</textarea>
	</div>
	<!-- Live Preview Box rechts -->
	<div id="b" class="split split-horizontal content">
		<div id="htmlOutput"></div>
	</div>
	
	<button id="save" class="btn btn-default" onclick="speichernAlsEntwurf();">Speichern</button>
</div>

<script src="js/split.js"></script>
<script src="js/remarkable.js"></script>
<script src="js/FileSaver.min.js"></script>
<script>
	
	Split(['#a', '#b'], {
		gutterSize: 8,
		cursor: 'col-resize'
	});
	
	
	var md = new Remarkable('full', {
		html:         true,         // Enable HTML tags in source
		xhtmlOut:     false,        // Use '/' to close single tags (<br />)
		breaks:       false,        // Convert '\n' in paragraphs into <br>
		langPrefix:   'language-',  // CSS language prefix for fenced blocks
		linkify:      true,         // autoconvert URL-like texts to links
		linkTarget:   '',           // set target to open link in
		
		// Enable some language-neutral replacements + quotes beautification
		typographer:  true,
		
		// Double + single quotes replacement pairs, when typographer enabled,
		// and smartquotes on. Set doubles to '«»' for Russian, '„“' for German.
		quotes: '“”‘’'
	});
	
	function speichernAlsEntwurf(){
		var oxid   = $('#oxid').val();
		var text   = $('#md').html();
		var valide = false;
		
		console.log(text);
		
		$.ajax({
			type:'get',
			url:'form.php',
			data:{
				fnc     : 'save',
				OXID    : oxid,
				text    : text,
				valide  : valide
			},
			
			
		});
	}


	
	function test(){
		var a=$('form#testform').serialize();
		$.ajax({
			type:'post',
			url:'form.php',
			data:a,
			beforeSend:function(){
				//launchpreloader();
			},
			complete:function(){
				//stopPreloader();
			},
			success:function(result){
				//console.log(result);
				console.log(jQuery.parseJSON(result));
				var res = jQuery.parseJSON(result);
				
				$('#md').html('');
				//$('#md').html(res.test + res.post + res.string);
				
				parseMD();
			}
		});
	}
	
	function parseMD() {
		document.getElementById('htmlOutput').className = 'preview markdown-body';
		document.getElementById('htmlOutput').innerHTML = md.render(document.getElementById('md').value);
	}
	
	function saveMD() {
		var content = document.getElementById('md').value;
		var blob = new Blob([content], {type: "text/plain;charset=utf-8"});
		saveAs(blob, 'document.md');
		return false;
	}
	
	function saveHTML() {
		var content = document.getElementById('htmlOutput').innerHTML
		var blob = new Blob([content], {type: "text/html;charset=utf-8"});
		saveAs(blob, 'document.html');
		return false;
	}
	
	function suchen(){
		//var a=$('form#testform').serialize();
		$( "#search" ).autocomplete({
				source: function(request, response) {
					$.ajax({
						type: 'post',
						url: 'form.php',
						dataType: "json",
						data: {
							term: request.term
						},
						success: function (data) {
							
							console.log(data);
							response(data);
							
						}
					})
				},
				minLenght :2,
				select: function (event, ui) {
					//log("Selected: " + ui.item.value + " aka " + ui.item.id);
				}
			}
		)
	}
	
	parseMD();


</script>

</body>
</html>

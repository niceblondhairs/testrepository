<script>

</script>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">

<head>
    <link rel="icon" href="img/favicon.ico.png">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <title>DataTables example - Bootstrap 4</title>


    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="css/dataTables.bootstrap4.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.2/css/responsive.bootstrap4.css"/>

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.js"></script>

    <script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.2/js/dataTables.responsive.js"></script>



    <!--
    <link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="http://www.datatables.net/rss.xml">
    <link rel="stylesheet" type="text/css" href="/media/css/site-examples.css?_=19472395a2969da78c8a4c707e72123a">
   <!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4-4.1.1/jq-3.3.1/dt-1.10.18/r-2.2.2/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="datatablebt.css">
    <link rel="stylesheet" type="text/css" href="datatable.css">
    <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <style type="text/css" class="init"></style>



    <!-- Kommentare sind im Quelltext sichtbar. type ist an einigen Stellen wichtig!!
     <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/af-2.3.0/b-1.5.2/fh-3.1.4/r-2.2.2/sc-1.5.0/sl-1.2.6/datatables.min.css"/>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/af-2.3.0/b-1.5.2/fh-3.1.4/r-2.2.2/sc-1.5.0/sl-1.2.6/datatables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4-4.1.1/jq-3.3.1/dt-1.10.18/r-2.2.2/datatables.min.js"></script>
    <!--
    <script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    -->


</head>

<body style="margin-left: 10px;margin-right: 10px;margin-top: 10px;">


<table id="table" class="compact stripe table table-striped table-bordered dataTable cell-border hover" style="width: 100%;" role="grid" aria-describedby="table_info">

    <thead class="text-center">

    <tr role="row">
        <th id="h0"></th>
        <th id="h1"></th>
        <th id="h2"></th>
        <th id="h3"></th>
        <th id="h4"></th>
        <th id="h5"></th>
        <th id="h6"></th>
        <th id="h7" style="display: none;"></th>
        <th id="h8" style="display: none;"></th>
        <th id="h9" style="display: none;"></th>
        <th id="h10" style="display: none;"></th>


    </tr>
    </thead>
    <tbody id="bodytable" style="cursor: crosshair; ">

    </tbody>
</table>

<div class="container">
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Vorschau</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div id="modal-preview" class="modal-body">
                    <p></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
</div>

<script>








    if(localStorage.rights!=='admin'){
        alert('Keine Rechte!');

    } else {

        $(document).ready(function () {
            var table = $('#table').DataTable({

                'pagingType' : 'full_numbers'
            });
            var searchID = document.getElementById('table_filter').children[0].lastChild.id = 'search';
            var x = window.location.search;
            console.log(x);
            if (x === '?lang=1') {
                var lang = '_1';
            } else if (x === '?lang=2') {
                var lang = '_2';
            } else {
                var lang = '';
            }
            console.log(lang);
            if (lang === '') {
                $('#h0').html("Artikelnummer");
                $('#h1').html("OXID");
                $('#h2').html("Kurzbeschreibung");
                $('#h3').html("Titel");
                $('#h4').html("Geändert von");
                $('#h5').html("Geprüft von");
                $('#h6').html("Validiert von");
                $('#h7').html("Beschreibung Eng Unsichtbar");
                $('#h8').html("Beschreibung Fr Unsichtbar");
                $('#h9').html("Titel Eng Unsichtbar");
                $('#h10').html("Titel Fr Unsichtbar");

                $('#search').attr('data-lang','de');


            } else if (lang === '_1') {
                $('#h0').html("Item Number");
                $('#h1').html("OXID");
                $('#h2').html("Short Description");
                $('#h3').html("Title");
                $('#h4').html("Changed by");
                $('#h5').html("Checked by");
                $('#h6').html("Validate by");
                $('#h7').html("Beschreibung De Unsichtbar");
                $('#h8').html("Beschreibung Fr Unsichtbar");
                $('#h9').html("Titel De Unsichtbar");
                $('#h10').html("Titel Fr Unsichtbar");
                $('#search').attr('data-lang','en');
            } else if (lang === '_2') {
                $('#h0').html("Numéro d'article");
                $('#h1').html("OXID");
                $('#h2').html("Brève Description");
                $('#h3').html("Titre");
                $('#h4').html("Changé par");
                $('#h5').html("Vérifié par");
                $('#h6').html("Validé par");
                $('#h7').html("Kurzbeschreibung De Unsichtbar");
                $('#h8').html("Kurzbeschreibung Eng Unsichtbar");
                $('#h9').html("Titel De Unsichtbar");
                $('#h10').html("Titel Eng Unsichtbar");
                $('#search').attr('data-lang','fr');

            }


            start();
            console.log(document.getElementById('table_filter').children[0].lastChild);
            console.log(document.getElementById('table_filter').children[0].lastChild.checked);

            console.log(searchID);
           // $('#search').attr('onkeyup','suche()');




        });




      /*  function suche(){
          //  $("table.table-bordered.dataTable td:last-child").css("display","none");
            var test = document.getElementById('search').value;
            var lang = $('#search').attr('data-lang');
            if((test ==='') && (lang ==='de')) {

                deutsch();
                console.log('test');
            } else if((test ==='') && (lang === 'en')){

                englisch();
                console.log('test eng');
            } else if((test ==='') && lang ==='fr'){

                französisch();
                console.log('test fr');
            }
          //  $("table.table-bordered.dataTable td:last-child").css("display","none");
        }
        */



        $('#table tbody').on('click', 'tr', function () {
            var x = $(this).toggleClass('selected');
            // console.log(x);
            var oxid = x[0].cells['1'].innerHTML;
            modal(oxid);

            //   window.open("http://oxid6.localhost:81/index.php?cl=details&cnid=" + oxid + "&anid=" + oxid + "&listtype=list&lang=" + lang, "Preview", "width=1200,height=750,scrollbars=yes,resizable=yes,status=0");

        });


    }

    function start() {
        $.ajax({


            type: 'get',
            url: "ArtikelID.php",
            data: {
                fnc: 'dataTable',
            },
            success: function (result) {
                var x = window.location.search;
                console.log(x);
                if (x === '?lang=1') {
                    var lang = '_1';
                    var lang_1 = '';
                    var lang_2 = '_2';
                } else if (x === '?lang=2') {
                    var lang = '_2';
                    var lang_1 = '';
                    var lang_2 = '_1';
                } else {
                    var lang = '';
                    var lang_1 = '_1';
                    var lang_2 = '_2';

                }
                console.log(lang);
                var table = $('#table').DataTable();
                table.on('search.dt', function() {
                    //number of filtered rows
                    // HALLOTEST = table.rows( { filter : 'applied'} ).nodes();
                    //filtered rows data as arrays
                    SuchErgebnisTabelle = table.rows( { filter : 'applied'} ).data();
                });



                var res = jQuery.parseJSON(result);
                console.log(res);
                console.log(res[1]['OXID']);
                for (i = 0; i < res.length; i++) {

                            table.row.add([
                            res[i]['OXARTNUM'],
                            res[i]['OXID'],
                            res[i]['OXSHORTDESC' + lang],
                            res[i]['OXTITLE' + lang],
                            res[i]['USERCHANGE' + lang] + ' -> ' + res[i]['OXUSERCHANGETIMESTAMP'],
                            res[i]['USERCHECK' + lang] + ' -> ' + res[i]['OXUSERCHECKTIMESTAMP'],
                            res[i]['USERVAL' + lang] + ' -> ' + res[i]['OXUSERVALTIMESTAMP'],
                            res[i]['OXSHORTDESC' + lang_1],
                            res[i]['OXSHORTDESC' + lang_2],
                            res[i]['OXTITLE' + lang_1],
                            res[i]['OXTITLE' + lang_2]
                        ]).draw();

                }
                for ( var i=7 ; i<11 ; i++ ) {
                    table.column(i).visible(false).draw();
                }



             //  $("table.table-bordered.dataTable td:last-child").css("display","none");
                // $('#h8').attr('style','visibility:hidden');
                var data = table.data();
                console.log(data);
                console.log(data.length);

                dataGLOBAL = data;

                $('#table tbody tr').attr({
                    'data-toggle': 'modal',
                    'data-target': '#mymodal'

                });


                var url = "https://bodynova.de/out/bodynova/img/lang/de.png";
                var string = '<button class="btn btn-default" style="margin-left: 5px; background-color: white; border: 1px solid #CCCCCC " onclick="deutsch()"><img src="' + url + '" /></button>';
                $('#table_length').append(string);

                var url = "https://bodynova.de/out/bodynova/img/lang/en.png";
                var string = '<button class="btn btn-default" style="margin-left: 5px; background-color: white; border: 1px solid #CCCCCC " onclick="englisch()"><img src="' + url + '" /></button>';
                $('#table_length').append(string);

                var url = "https://bodynova.de/out/bodynova/img/lang/fr.png";
                var string = '<button class="btn btn-default" style="margin-left: 5px; background-color: white; border: 1px solid #CCCCCC " onclick="französisch()"><img src="' + url + '" /></button>';
                $('#table_length').append(string);


            }
        });

    }
    function modal(oxid) {
        console.log('TEST');


        console.log(oxid);
        $.ajax({
            //dataType: 'json',
            type: 'get',
            url: 'ArtikelID.php',
            data: {
                fnc: 'modalForList',
                oxid: oxid,

            },
            success: function (result) {


                var res = jQuery.parseJSON(result);
                // console.log(result);
                var pic = res[0]['OXPIC1'];
                var testlang = $('#search').attr('data-lang');
                // x = new URLSearchParams(window.location.search);

                // var testlang = x.get('lang');
                console.log(testlang);
                if(testlang === 'en'){
                    testlang = '_1';
                } else if(testlang === 'fr') {
                    testlang = '_2';
                } else testlang = '';

                var text = res[0]['OXLONGDESC' +testlang ];

                console.log(pic);

                $('#modal-preview').html(text);
                $('#modal-preview').prepend('<img style="height: 400px;"  id="theImg" src="../out/pictures/master/product/1/' + pic + '" />');
                //  $('#modal-preview').prepend('<img style="height: 400px;"  id="theImg" src="../out/pictures/master/product/1/kitefix_self-adhesive_dacron_1.jpg" />');

                $('#myModal').modal();

                // var text = res[OXPIC1];


            }
        });

    }



    function deutsch(){

        var SuchEingabe = $('#search').val();
        $.ajax({
            type: 'get',
            url: 'ArtikelID.php',
            data: {
                fnc: 'dataTable'
            },
            success: function (result) {


                $('#h0').html("Artikelnummer");
                $('#h1').html("OXID");
                $('#h2').html("Kurzbeschreibung");
                $('#h3').html("Titel");
                $('#h4').html("Geändert von");
                $('#h5').html("Geprüft von");
                $('#h6').html("Validiert von");

                var res = jQuery.parseJSON(result);
                //  console.log(res);

                $('#tbody').html('');
                var table = $('#table').DataTable();
                table.clear().draw();


                for (i = 0; i < res.length; i++) {

                    table.row.add([
                        res[i].OXARTNUM,
                        res[i].OXID,
                        res[i].OXSHORTDESC,
                        res[i].OXTITLE,
                        res[i].USERCHANGE + ' -> ' + res[i].OXUSERCHANGETIMESTAMP,
                        res[i].USERCHECK + ' -> ' + res[i].OXUSERCHECKTIMESTAMP,
                        res[i].USERVAL + ' -> ' + res[i].OXUSERVALTIMESTAMP,
                        res[i].OXSHORTDESC_1,
                        res[i].OXSHORTDESC_2,
                        res[i].OXTITLE_1,
                        res[i].OXTITLE_2
                    ]).draw();

                }
                // table.column(7).visible(false).draw();
              //  $("table.table-bordered.dataTable td:last-child").css("display","none");
                $('search').val(SuchEingabe);


            },
            error: function (result) {
                console.log(result);
                console.log('ERROR');
            }
        });


    }

    function englisch(){
        var SuchEingabe = $('#search').val();
        $.ajax({
            type: 'get',
            url: 'ArtikelID.php',
            data: {
                fnc: 'dataTable'
            },
            success: function (result) {


                $('#h0').html("Item Number");
                $('#h1').html("OXID");
                $('#h2').html("Short Description");
                $('#h3').html("Title");
                $('#h4').html("Changed by");
                $('#h5').html("Checked by");
                $('#h6').html("Validate by");
                var res = jQuery.parseJSON(result);
              //  console.log(res);

                $('#tbody').html('');
                var table = $('#table').DataTable();
                table.clear().draw();


                for (i = 0; i < res.length; i++) {

                    table.row.add([
                        res[i].OXARTNUM,
                        res[i].OXID,
                        res[i].OXSHORTDESC_1,
                        res[i].OXTITLE_1,
                        res[i].USERCHANGE_1 + ' -> ' + res[i].OXUSERCHANGETIMESTAMP,
                        res[i].USERCHECK_1 + ' -> ' + res[i].OXUSERCHECKTIMESTAMP,
                        res[i].USERVAL_1 + ' -> ' + res[i].OXUSERVALTIMESTAMP,
                        res[i].OXSHORTDESC,
                        res[i].OXSHORTDESC_2,
                        res[i].OXTITLE,
                        res[i].OXTITLE_2
                    ]).draw();

                }
                // table.column(7).visible(false).draw();
                $('search').val(SuchEingabe);


            },
            error: function (result) {
                console.log(result);
                console.log('ERROR');
            }
        });
    }



    function französisch(){

        var SuchEingabe = $('#search').val();
        $.ajax({
            type: 'get',
            url: 'ArtikelID.php',
            data: {
                fnc: 'dataTable'

            },
            success: function (result) {


                $('#h0').html("Numéro d'article");
                $('#h1').html("OXID");
                $('#h2').html("Brève Description");
                $('#h3').html("Titre");
                $('#h4').html("Changé par");
                $('#h5').html("Vérifié par");
                $('#h6').html("Validé par");
                var res = jQuery.parseJSON(result);
                //  console.log(res);

                $('#tbody').html('');
                var table = $('#table').DataTable();
                table.clear().draw();


                for (i = 0; i < res.length; i++) {

                    table.row.add([
                        res[i].OXARTNUM,
                        res[i].OXID,
                        res[i].OXSHORTDESC_2,
                        res[i].OXTITLE_2,
                        res[i].USERCHANGE_2 + ' -> ' + res[i].OXUSERCHANGETIMESTAMP,
                        res[i].USERCHECK_2 + ' -> ' + res[i].OXUSERCHECKTIMESTAMP,
                        res[i].USERVAL_2 + ' -> ' + res[i].OXUSERVALTIMESTAMP,
                        res[i].OXSHORTDESC,
                        res[i].OXSHORTDESC_1,
                        res[i].OXTITLE,
                        res[i].OXTITLE_1
                    ]).draw();

                }
               // table.column(7).visible(false).draw();

               // $("table.table-bordered.dataTable td:last-child").css("display","none");
                $('search').val(SuchEingabe);

            },
            error: function (result) {
                console.log(result);
                console.log('ERROR');
            }
        });
    }
    // console.log($('tbody tr').children()[1].innerHTML);



</script>


</body>
</html>
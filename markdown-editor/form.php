<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 23.07.18
 * Time: 05:48
 */
require dirname(__DIR__).'/../bootstrap.php';

if(empty($_GET)){
	die(json_encode(array('error' => 'Keine Parameter übergeben!')));
}


$suchwort = null;
$lang = null;
$return = null;
$suchergebnis = null;

if(!empty($_GET['lang'])){
	$lang = $_GET['lang'];
}else{
	$lang = '0';
}

// User eingabe und Suche
if(!empty($_GET['search']) && $_GET['fnc'] === 'suche'){
	$suchwort = $_GET['search'];
	$suchergebnis = sucheArtikel($suchwort, $lang);
	foreach ($suchergebnis as $ergebnis){
		$return[] = array(
			'OXID' => $ergebnis['OXID'],
			'Title'=> $ergebnis['Title'],
			'OXARTNUM' => $ergebnis['OXARTNUM'],
			'request' => $_GET
		);
	}
	die(json_encode($return));
}
// User hat aus dem Dropdown einen speziellen Artikel ausgewählt
if(!empty($_GET['OXID']) && $_GET['fnc'] === 'getArtikel'){
	$OXID = $_GET['OXID'];
	$ergebnis = getArtikel($OXID);
	if($ergebnis['OXPARENTID'] !== ''){
		$ergebnis = getArtikel($ergebnis['OXPARENTID']);
	}
	$arttext = getArtikelText($ergebnis['OXID']);
	//$validerText = validate($arttext['OXLONGDESC']);
	$return = array('Artikel' => $ergebnis, 'Artikeltext' => $arttext,'request' => $_GET);
	die(json_encode($return));
}
// TO DO: User hat Text geändert und will diesen abspeichern als Entwurf



/* Tidy Kram kann weg
	#$html = $arttext[0]['OXLONGDESC'];
	
	$config = array(
		'wrap'                         => 0,
		'lower-literals'               => 1,
		'preserve-entities'            => 1,
		'drop-empty-paras'             => 0
	);
	
	//$tidy = new tidy;
	
	//$tidy->parseString($html, $config, 'utf8');
	
	//$tidy->cleanRepair();
*/
function validate($html){
	$config = array(
		'wrap' => 0,
		'lower-literals' => 1,
		'preserve-entities' => 1,
		'drop-empty-paras' => 0
	);
	$tidy = new tidy;
	
	$tidy->parseString($html, $config, 'utf8');
	
	$tidy->cleanRepair();
	
	return tidy_get_output($tidy);
}


function formatReturn($lang, $artikel = null, $artikeltext = null, $request){
	$field = null;
	if($lang > 0){
		$field = '_'.$lang;
	}
	
	$titel = null;
	$text  = null;
	switch($lang)
	{
		case (0):
			$titel = $artikel['OXTITLE'];
			$text  = $artikeltext['OXLONGDESC'];
			break;
		case (1):
			$titel = $artikel['OXTITLE_1'];
			$text  = $artikeltext['OXLONGDESC_1'];
			break;
		case(2):
			$titel = $artikel['OXTITLE_2'];
			$text  = $artikeltext['OXLONGDESC_2'];
			break;
		default:
			$titel = $artikel['OXTITLE'];
			$text  = $artikeltext['OXLONGDESC'];
			break;
	}
	
	$formatArtikel = array(
		'OXID'          => $artikel['OXID'],
		'OXPARENTID'    => $artikel['OXPARENTID'],
		'OXACTIVE'      => $artikel['OXACTIVE'],
		'Titel'         => $titel,
		'OXTITLE'       => $artikel['OXTITLE'],
		'OXTITLE_1'     => $artikel['OXTITLE_1'],
		'OXTITLE_2'     => $artikel['OXTITLE_2'],
		'Text'          => $text
	);
	$formatArtikelText = array(
		'OXID'          => $artikeltext['OXID'],
		'OXLONGDESC'    => $artikeltext['OXLONGDESC'],
		'OXLONGDESC_1'  => $artikeltext['OXLONGDESC_1'],
		'OXLONGDESC_2'  => $artikeltext['OXLONGDESC_2']
	);
	return array('Artikel' => $artikel, 'Artikeltext' => $artikeltext, 'request' => $request);
}



function getArtikel($OXID){
	try{
		$oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb();
		$oDb->setFetchMode(2);
		$sSql = 'select * from oxarticles where OXID = ?';//.$oDb->quote($suchwort);
		$suchergebnis = $oDb->getAll($sSql, array($OXID));
	}catch(Exception $e ){
		print_r($e);
	}
	$return['ergebnis'] = $suchergebnis[0];
	
	return $suchergebnis[0];
	//return $return;
}

function getArtikelText($OXID){
	
	try{
		$oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb();
		$oDb->setFetchMode(2);
		$sSql = 'select * from oxartextends where OXID = ?';//.$oDb->quote($suchwort);
		$suchergebnis = $oDb->getAll($sSql, array($OXID));
	}catch(Exception $e ){
		print_r($e);
	}
	$return['ergebnis'] = $suchergebnis[0];
	return $suchergebnis[0];
	//return $return;
}

function sucheArtikel($suchwort, $lang){
	$field = null;
	if($lang > 0){
		$field = '_'.$lang;
	}
	try{
		$oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb();
		$oDb->setFetchMode(2);
		$sSql = "SELECT OXID, OXARTNUM, CONCAT(OXTITLE".( $field !== null ? $field : '' ).", OXVARSELECT".( $field !== null ? $field : '' ).", ' (', OXARTNUM, ') ') as Title FROM oxarticles WHERE CONCAT(OXTITLE".( $field !== null ? $field : '' ).", '', OXVARSELECT".( $field !== null ? $field : '' ).", OXARTNUM) LIKE ?";//.$oDb->quote($suchwort);
		$suchergebnis = $oDb->getAll($sSql, array('%'.$suchwort.'%'));
	}catch(Exception $e ){
		print_r($e);
	}
	//$return['ergebnis'] = $suchergebnis[0];
	return $suchergebnis; //array('Title'=>$sSql);
}
